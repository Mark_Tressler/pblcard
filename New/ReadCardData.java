// Mark Tressler

//This code reads in the card data from carddata.txt
//1 card is an arraylist,  with 5 data fields
//each card is stored in an arraylist "deck"

import java.util.*;
import java.io.*;

public class ReadCardData {

    public static void main(String args[]) throws IOException {
        File cardData = new File("carddata.txt");	//File to read in
		Scanner sc = new Scanner(cardData);			//Creates a scanner for that file

		ArrayList<String> card = new ArrayList<String>();
		ArrayList<ArrayList<String>> deck = new ArrayList<ArrayList<String>>();

		boolean run = true;	//Controls the build process; sets to false when the scanner reaches end of file
		do{
			for(int a = 1; a <= 5; a++){	//Builds a card, 5 lines each
				card.add(sc.nextLine());
			}
			try{	//Tries to skip a line; if it cannot, kill the build process
				sc.nextLine();
			}catch(NoSuchElementException e){
				run = false;
			}
			deck.add(card);	//Add card to deck
			card = new ArrayList<String>();	//Reset card
		}while(run);



		//By this point, deck is fully built
		System.out.println(deck);	//Print deck


    }

}

