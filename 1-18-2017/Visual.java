//Graphics

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.sound.sampled.*;
import java.util.*;
import java.io.*;
import javax.imageio.ImageIO;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.awt.print.PrinterException;

public class Visual extends JFrame implements KeyListener, ComponentListener, SettingsSetListener, CardClickListener, TableClickListener {
   private CalendarCardDataModel model;
   private CardCell cell;
   private JLabel label;
   private JPanel calContainer, panel4;
   private CardVisual cardContainer;
   private JLabel cardInstr;
   private JMenuBar menuBar;
   private JTextArea textField;
   private JButton r, g, b, y, p, bl, nextCardBt, prevCardBt, b1, b2, startStop, prev, next, X;
   private CardTable table;
   private JScrollPane pane;
   private CardsManager manager;
   int center = (int)(WIDTH * 0.82);
   private Font title = new Font("Serif", Font.BOLD, 40);
   private int x = new Random().nextInt(6), highLight = -1, sliderValue;
   private String music = "";
   private int yllwIndex = 0, redIndex = 0, blueIndex = 0, greenIndex = 0, purpleIndex = 0, blackIndex = 0, currentCard = -1, loop = 0;
   private boolean selected = false, stop = false, changeSize = false, reshowCard = false;
   private int[] points;
   private Card current;
   private AudioInputStream audioInputStream, audioInputStream1;
   private Clip clip3, clip, clip2;
   public final static Color hotpink = new Color( 255, 100, 180 );
   public final static Color neoGreen = new Color( 57,255,20 );
   public Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
   private double height = (int) screenSize.getHeight(), width = (int) screenSize.getWidth();
   public static boolean visOpened = true;

   public Visual() {
      super("PBL");
      this.setTitle("PBL Calendar");
      try {
         this.setIconImage(ImageIO.read(new File("res/ic_calendar.png")));
      } 
      catch (Exception e) {
      
      }
      manager = new CardsManager();    // Builds new CardsManager
      
      //////  Run requisite methods  //////
      
      calendar();
      card();
//      music();     Music method; depreciated, do not uncomment
      menu();
      
      
      //////  Create card adding interface  //////
      
      // Build card objects
         //Next Card
      nextCardBt = new JButton("Next Card");                                        //Set text on button
      nextCardBt.setFont(new Font("TimesRoman", Font.BOLD, (int)(height/50)));      //Set font and font size for button
      nextCardBt.setForeground(Color.BLACK);                                        //Set text color
      nextCardBt.setFocusPainted(false);                                            //Start the button unfocused
         //Previous Card
      prevCardBt = new JButton("Previous Card");
      prevCardBt.setFont(new Font("TimesRoman", Font.BOLD, (int)(height/50)));
      prevCardBt.setForeground(Color.BLACK);
      prevCardBt.setFocusPainted(false);
         //Required Cards (YELLOW)
      y = new JButton("Required Cards");
      y.setFont(new Font("Serif", Font.BOLD, (int)(height/64)));
      y.setForeground(Color.MAGENTA);
      y.setFocusPainted(false);
         //Content Workshops (RED)
      r = new JButton("Content Workshops");
      r.setFont(new Font("Serif", Font.BOLD, (int)(height/70)));
      r.setForeground(Color.GREEN);
      r.setFocusPainted(false);
         //Expert Workshops (GREEN)
      g = new JButton("Expert Workshops");
      g.setFont(new Font("Serif", Font.BOLD, (int)(height/64)));
      g.setForeground(Color.RED);
      g.setFocusPainted(false);
         //Assessments (PURPLE)
      p = new JButton("Assessments");
      p.setFont(new Font("Serif", Font.BOLD, (int)(height/64)));
      p.setForeground(Color.YELLOW);
      p.setFocusPainted(false);
         //Emerging Workshops (BLUE)
      b = new JButton("Emerging Workshops");
      b.setFont(new Font("Serif", Font.BOLD, (int)(height/77.5)));
      b.setForeground(Color.ORANGE);
      b.setFocusPainted(false);
         //Project Time (BLACK)
      bl = new JButton("Project Time");
      bl.setFont(new Font("Serif", Font.BOLD, (int)(height/64)));
      bl.setForeground(Color.WHITE);
      bl.setFocusPainted(false);
      
      
      setSize((int)width,(int)height);
      add(nextCardBt);
      add(prevCardBt);
      add(y);
      add(r);
      add(g);
      add(p);
      add(b);
      add(bl);
      setLayout(null);
      setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
      
      
      // Add funtionality to buttons
      
         //Give functionality to Next Card button (If button is clicked, move selected card's index up one place)
      nextCardBt.addActionListener(
         new ActionListener()
         {
            public void actionPerformed(ActionEvent e)
            {
               switch(currentCard) {
                  case 1:                                                                                                  //If selected card is Required Cards (YELLOW)
                     yllwIndex = (yllwIndex < (manager.getMax(CardsManager.YELLOW) - 1)) ? yllwIndex + 1 : 0;
                     updateCard();
                     break;
                  case 2:                                                                                                  //If selected card is Content Workshops (RED)
                     redIndex = (redIndex < (manager.getMax(CardsManager.RED) - 1)) ? redIndex + 1 : 0;
                     updateCard();
                     break;
                  case 3:                                                                                                  //If selected card is Expert Workshops (GREEN)
                     greenIndex = (greenIndex < (manager.getMax(CardsManager.GREEN) - 1)) ? greenIndex + 1 : 0;
                     updateCard();
                     break;
                  case 4:                                                                                                  //If selected card is Assessments (PURPLE)
                     purpleIndex = (purpleIndex < (manager.getMax(CardsManager.PURPLE) - 1)) ? purpleIndex + 1 : 0;
                     updateCard();
                     break;
                  case 5:                                                                                                  //If selected card is Emerging Workshops (BLUE)
                     blueIndex = (blueIndex < (manager.getMax(CardsManager.BLUE) - 1)) ? blueIndex + 1 : 0;
                     updateCard();
                     break;
                  case 6:                                                                                                  //If selected card is Project Time (BLACK)
                     blackIndex = (blackIndex < (manager.getMax(CardsManager.BLACK) - 1)) ? blackIndex + 1 : 0;
                     updateCard();
                     break;
               }
            }
         });
         
         //Give functionality to Prev Card button (If button is clicked, move selected card's index back one place)
      prevCardBt.addActionListener(
         new ActionListener()
         {
            public void actionPerformed(ActionEvent e)
            {
               switch(currentCard) {
                  case 1:                                                                                                  //If selected card is Required Cards (YELLOW)
                     yllwIndex = (yllwIndex > 0) ? yllwIndex - 1 : manager.getMax(CardsManager.YELLOW) - 1;
                     updateCard();
                     break;
                  case 2:                                                                                                  //If selected card is Content Workshops (RED)
                     redIndex = (redIndex > 0) ? redIndex - 1 : manager.getMax(CardsManager.RED) - 1;
                     updateCard();
                     break;
                  case 3:                                                                                                  //If selected card is Expert Workshops (GREEN)
                     greenIndex = (greenIndex > 0) ? greenIndex - 1 : manager.getMax(CardsManager.GREEN) - 1;
                     updateCard();
                     break;
                  case 4:                                                                                                  //If selected card is Assessments (PURPLE)
                     purpleIndex = (purpleIndex > 0) ? purpleIndex - 1 : manager.getMax(CardsManager.PURPLE) - 1;
                     updateCard();
                     break;
                  case 5:                                                                                                  //If selected card is Emerging Workshops (BLUE)
                     blueIndex = (blueIndex > 0) ? blueIndex - 1 : manager.getMax(CardsManager.BLUE) - 1;
                     updateCard();
                     break;
                  case 6:                                                                                                  //If selected card is Project Time (BLACK)
                     blackIndex = (blackIndex > 0) ? blackIndex - 1 : manager.getMax(CardsManager.BLACK);
                     updateCard();
                     break;
               }
            }
         });
         
         //If YELLOW button is pressed, set currentCard to 1 (Required Cards)
      y.addActionListener(
         new ActionListener() {
            public void actionPerformed(ActionEvent e) {
               currentCard = 1;
               updateCard();
            }
         });
         //If RED button is pressed, set currentCard to 2 (Content Workshops)
      r.addActionListener(
         new ActionListener() {
            public void actionPerformed(ActionEvent e) {
               currentCard = 2;
               updateCard();
            }
         });
         //If GREEN button is pressed, set currentCard to 3 (Expert Workshops)
      g.addActionListener(
         new ActionListener() {
            public void actionPerformed(ActionEvent e) {
               currentCard = 3;
               updateCard();
            }
         });
         //If PURPLE button is pressed, set currentCard to 4 (Assessments)
      p.addActionListener(
         new ActionListener()
         {
            public void actionPerformed(ActionEvent e)
            {
               currentCard = 4;
               updateCard();
            }
         });
         //If BLUE button is pressed, set currentCard to 5 (Emerging Workshops)
      b.addActionListener(
         new ActionListener() {
            public void actionPerformed(ActionEvent e) {
               currentCard = 5;
               updateCard();
            }
         });
         //If BLACK button is pressed, set currentCard to 6 (Project Time)
      bl.addActionListener(
         new ActionListener() {
            public void actionPerformed(ActionEvent e) {
               currentCard = 6;
               updateCard();
            }
         });
         
         
         
      //Set card background colors
         //Set Next Card button background to white   
      nextCardBt.setLayout(null);
      nextCardBt.setBackground(Color.white);
         //Set Prev Card button background to white
      prevCardBt.setLayout(null);
      prevCardBt.setBackground(Color.white);
      
         //Set YELLOW button background to yellow
      y.setBackground(Color.yellow);
      y.setLayout(null);
         //Set RED button background to red
      r.setBackground(Color.red);
      r.setLayout(null);
         //Set GREEN button background to green
      g.setBackground(Color.green);
      g.setLayout(null);
         //Set PURPLE button background to purple (CUSTOM COLOR, VALUE #AA00FF)
      p.setBackground(Color.decode("#AA00FF"));
      p.setLayout(null);
         //Set BLUE button background to blue
      b.setBackground(Color.blue);
      b.setLayout(null);
         //Set BLACK button background to black
      bl.setBackground(Color.black);
      bl.setLayout(null);   
      
      //////  Add window functionality  //////
      
      //Add keylisteners to window
      table.addKeyListener(this);
      addKeyListener(this);
      addComponentListener(this);
      addWindowListener(
         new WindowAdapter() {
            public void windowClosing(WindowEvent we)    //If window close X is clicked
            { 
               String ObjButtons[] = {"Yes", "No"};
               int PromptResult = JOptionPane.showOptionDialog(null,"Are you sure you want to exit?","PBL Calendar: Close App?",JOptionPane.DEFAULT_OPTION,JOptionPane.WARNING_MESSAGE,null,ObjButtons,ObjButtons[1]);               
               if(PromptResult==JOptionPane.YES_OPTION) //If yes, terminate program
               {
                  System.exit(0);   //Close window, exit program
               } //If no, do nothing. Return to program
            }
         });
      setVisible(true); //Make the screen visible
   }
   
   public void onCardClicked() {
      highLight();
   }
   
   public void onTableClicked(int row, int col) {
      if (selected) {
         if (model.checkPoints(row, col)) {
            table.setData(current, row, col);
            table.repaint();
            highLight();
         }
      } 
      else
         points = new int[]{row, col};
   }
   
   public void keyTyped(KeyEvent ke) {
   
   }

    /** heightandle the key-pressed event from the cell. */
   public void keyPressed(KeyEvent ke) {
      if (points != null && String.valueOf(ke.getKeyChar()).equals("")) {
         //table.setData(new Card(0, "DELETE", "DELETE", "DELETE", "DELETE", 0, 0, "DELETE"), points[0], points[1]);
         table.delete(points[0], points[1]);
         table.repaint();
      }
   }

   public void keyReleased(KeyEvent e) {
       
   }
   
   public void componentResized(ComponentEvent e) {
      height = e.getComponent().getHeight();
      width = e.getComponent().getWidth();
      changeSize();
   }
   
   public void componentMoved(ComponentEvent e) {
      
   }
   
   public void componentShown(ComponentEvent e) {
      
   }
   
   public void componentHidden(ComponentEvent e) {
   
   }
   
   public void settingsSet() {
      table.repaint();
   }
   
   public void updateCard() {   
      switch(currentCard) {
         case 1:
            current = manager.getYellow(yllwIndex);
            break;
         case 2: 
            current = manager.getRed(redIndex);
            break;
         case 3:
            current = manager.getGreen(greenIndex);
            break;
         case 4:
            current = manager.getPurple(purpleIndex);
            break;
         case 5:
            current = manager.getBlue(blueIndex);
            break;
         case 6:
            current = manager.getBlack(blackIndex);
            break;
         default:
            break;              
      }
      
      if(highLight%2 == 0)
         highLight();
      
      showCard();
   }
   
   private void showCard() {
      cardContainer.setVisible(true);
      if (current != null)
         cardContainer.setData(current);
   }
   
   private void highLight() {
      highLight++;
      if(highLight % 2 == 0) {
         selected = true;
         cardContainer.setBorder(BorderFactory.createLineBorder(Color.decode(current.getColor()), 10));
      } 
      else {
         selected = false;
         cardContainer.setBorder(BorderFactory.createLineBorder(Color.WHITE, 10));
      }
   }
   
   private void changeSize() {
      //Set bounds and font for card and color menu:
      y.setLocation((int)(width/1.45),(int)(height/1.636));
      y.setSize((int)width/10,(int)height/11);
      r.setLocation((int)(width/1.266),(int)(height/1.636));
      r.setSize((int)width/10,(int)height/11);
      g.setLocation((int)(width/1.45),(int)(height/1.420));
      g.setSize((int)width/10,(int)height/11);
      p.setLocation((int)(width/1.266),(int)(height/1.420));
      p.setSize((int)width/10,(int)height/11);
      b.setLocation((int)(width/1.45),(int)(height/1.256));
      b.setSize((int)width/10,(int)height/11);
      bl.setLocation((int)(width/1.266),(int)(height/1.256));
      bl.setSize((int)width/10,(int)height/11);
      y.setFont(new Font("Serif", Font.BOLD, (int)(height/64)));
      r.setFont(new Font("Serif", Font.BOLD, (int)(height/70)));
      g.setFont(new Font("Serif", Font.BOLD, (int)(height/64)));
      p.setFont(new Font("Serif", Font.BOLD, (int)(height/64)));
      b.setFont(new Font("Serif", Font.BOLD, (int)(height/77.5)));
      bl.setFont(new Font("Serif", Font.BOLD, (int)(height/64)));
      label.setBounds((int) (width / 32), 0, (int) ((width/1.9) - (width / 32) - (width / 32)), (int) (height / 53));
      prev.setBounds(0, 0, (int) (width / 32), (int) (height / 53));
      next.setBounds((int) ((width/1.9) - (width / 32)), 0, (int) (width / 32), (int) (height / 53));
      cardContainer.setBounds((int)(width/1.514), (int)(height/27), (int)(width/3.918), (int)(height/2.16));
      cardContainer.setSize((int) width, (int) height); //Method to set the content of the card to correct size
      cardInstr.setBounds((int)(width/1.4),(int)(height/1.792),(int)(width/4.8),(int)(height/21.6));
      cardInstr.setFont(new Font("Serif", Font.BOLD, (int)(height/27)));
      cardInstr.setFont(new Font("Serif", Font.BOLD, (int)(height/27)));
      nextCardBt.setLocation((int)(width/1.25),(int)(height/1.958));
      nextCardBt.setSize((int)width/9,(int)height/21);
      nextCardBt.setFont(new Font("TimesRoman", Font.BOLD, (int)(height/50)));
      prevCardBt.setFont(new Font("TimesRoman", Font.BOLD, (int)(height/50)));
      prevCardBt.setLocation((int)(width/1.5),(int)(height/1.958));
      prevCardBt.setSize((int)width/9,(int)height/21);


/*      
      //Set bounds and font for music
      startStop.setLocation((int)(width/3.69),(int)(height/1.26));
      startStop.setSize((int)(width/5.4857),(int)(height/13.5));
      startStop.setFont(new Font("Rockwell", Font.BOLD, (int)(height/54)));
*/ 
  
     /*
      //Set bounds and heights for calendar and menu:
      calContainer.setBounds(0, (int) height / 40, (int) (width/1.9), (int)(height/1.31204819277108));  
      table.setRowHeight((int) (height / 8));
      pane.setBounds(0, (int) (height / 52.9), (int) (width / 1.9), (int) ((height/1.301204819277108) - (height / 20)));
      menuBar.setBounds(0, 0, (int) width, (int) height / 40);
     */
     
      calContainer.setBounds(0, (int) height / 40, (int) (width/1.6), (int)(height));  
      table.setRowHeight((int) (height / 5.8));
      pane.setBounds(0, (int) (height / 52.9), (int) (width / 1.6), (int) ((height)));
      menuBar.setBounds(0, 0, (int) width, (int) height / 40);
     
     
     
      
      //Set bounds for other items
      panel4.setSize((int)(width/3),(int)(height/5));
      panel4.setLocation((int)(width/3.5),(int)(height/2.561));
      textField.setFont(new java.awt.Font("TIMES NEW ROMAN", Font.ITALIC | Font.BOLD, (int)(height/77.14)));
      X.setLocation((int)(width/1.6725),(int)(height/2.76));
      X.setSize((int)width/45,(int)height/35);
   }

   private void card() {
      cardContainer = new CardVisual(this, height, width);
      cardContainer.setLayout(null);
      cardContainer.setBounds((int)(width/1.514), (int)(height/27), (int)(width/3.918), (int)(height/2.16));
      cardContainer.setBackground(Color.WHITE);
      cardContainer.setOpaque(true);
      cardContainer.setVisible(false);
      add(cardContainer);
      cardInstr = new JLabel();
      cardInstr.setFont(new Font("Serif", Font.BOLD, (int)(height/27)));
      cardInstr.setText("Select a card type");
      cardInstr.setOpaque(true);
      add(cardInstr);
   }
   
   private void calendar() {      
      prev = new JButton("<");
      prev.setFocusPainted(false);
      prev.setBounds(0, 0, (int) (width / 32), (int) (height / 53));
      prev.addActionListener(
         new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
               model.previousMonth();
            }
         });
   
      next = new JButton(">");
      next.setFocusPainted(false);
      next.setBounds((int) ((width/1.9) - (width / 32)), 0, (int) (width / 32), (int) (height / 53));
      next.addActionListener(
         new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
               model.nextMonth();
            }
         });
         
      label = new JLabel();
      label.setBounds((int) (width / 32), 0, (int) ((width/1.9) - (width / 32) - (width / 32)), (int) (height / 53));
      label.setHorizontalAlignment(SwingConstants.CENTER);
   
      calContainer = new JPanel();
      calContainer.setBounds(0, (int) height / 30, (int) (width/1.9), (int)(height/1.301204819277108));
      calContainer.setLayout(null);
      calContainer.add(prev);
      calContainer.add(label);
      calContainer.add(next);
      
      cell = new CardCell();
      table = new CardTable(model = new CalendarCardDataModel(new String[]{"Sun","Mon","Tue","Wed","Thu","Fri","Sat"}, label), this);
      model.setTable(table);
      table.setRowSelectionAllowed(false);
      pane = new JScrollPane(table);
      pane.setBorder(BorderFactory.createEmptyBorder());
      calContainer.add(pane);
      add(calContainer);
      model.updateCalendar();
   }
   
   
   public void music() {
      startStop = new JButton("Start Music");
      startStop.setForeground(neoGreen);
      startStop.setBackground(Color.BLACK);
      startStop.setFocusPainted(false);
      startStop.setBorder(BorderFactory.createLineBorder(neoGreen,2));
      add(startStop);
      JPanel panel3 = new JPanel(new GridLayout(1, 0, 1, 0));
      panel3.setSize((int)(width/3),(int)(height/20));                 
      panel3.setLocation((int)(width/2.9538),(int)(height/1.14));
      panel3.setOpaque(true);
      add(panel3);
      JSlider slider = new JSlider(JSlider.HORIZONTAL, 0, 20, 1);  
      slider.setMinorTickSpacing(5);  
      slider.setMajorTickSpacing(1);  
      slider.setPaintTicks(true);  
      slider.setPaintLabels(true);
      slider.setFont(new Font("Rockwell", Font.BOLD, (int)(height/80)));
      slider.setOpaque(true);
      //slider.setVisible(true);
      panel3.add(slider);
      panel3.setVisible(false);
      JLabel volume = new JLabel();
      volume.setFont(new Font("Serif", Font.BOLD, (int)(height/50)));
      volume.setText("Volume");
      volume.setBounds((int)(width/2.1),(int)(height/1.2),(int)(width/4.8),(int)(height/21.6));
      volume.setOpaque(true);    
      add(volume);
      volume.setVisible(false);
            
      startStop.addActionListener(
         new ActionListener()
         {
            public void actionPerformed(ActionEvent e)
            {
               try{
                  if(loop % 2 == 0) {
                     if(loop % 4 == 0)
                        music = "res/Music1.wav";
                     else if(loop == 30)
                        music = "res/Music3.wav";
                     else
                        music = "res/Music.wav";
                     slider.setVisible(true);
                     panel3.setVisible(true);
                     volume.setVisible(true);
                  
                     audioInputStream = AudioSystem.getAudioInputStream(new File(music));
                     clip3 = AudioSystem.getClip();
                     clip3.open(audioInputStream);
                     clip3.loop(Clip.LOOP_CONTINUOUSLY);
                     FloatControl gainControl = (FloatControl)clip3.getControl(FloatControl.Type.MASTER_GAIN);
                     gainControl.setValue(-20);
                     clip3.start();
                     slider.setValue(0);
                     gainControl.setValue(slider.getValue()-14);
                     slider.addChangeListener(
                        new ChangeListener() {
                           public void stateChanged(ChangeEvent e) {
                              sliderValue = ((JSlider)e.getSource()).getValue();
                              if(sliderValue-14>gainControl.getMaximum())
                                 gainControl.setValue(gainControl.getMaximum());
                              else
                                 gainControl.setValue(sliderValue-14);
                           }
                        });
                     
                     startStop.setForeground(Color.RED);
                     startStop.setBackground(Color.BLACK);
                     startStop.setBorder(BorderFactory.createLineBorder(Color.RED,2));
                     startStop.setText("Stop Music");
                     loop++;
                  }
                  else{
                     slider.setVisible(false);
                     panel3.setVisible(false);
                     volume.setVisible(false);
                     clip3.stop();
                     startStop.setText("Start Music");
                     startStop.setForeground(neoGreen);
                     startStop.setBackground(Color.BLACK);
                     startStop.setBorder(BorderFactory.createLineBorder(neoGreen,2));
                     loop++;
                  }
               } 
               catch (Exception f) {
                  System.err.println(f.getMessage());
               } 
            }
         });
      startStop.setLayout(null);
      startStop.setLocation((int)(width/3.69),(int)(height/1.3));
      startStop.setSize((int)(width/5.4857),(int)(height/13.5));
   }
   
   
   public static void getReadyToRumble(){
      try {
         Thread.sleep(5000);
      } 
      catch(InterruptedException ex) {
         Thread.currentThread().interrupt();
      }
      UpdateCalendar update = new UpdateCalendar(); 
      try{
         update.Update();  
      }
      catch(IOException e){}
   }
   
   private void menu() {
      panel4 = new JPanel(new GridLayout(1,1,1,1));
      panel4.setSize((int)(width/3),(int)(height/5));
      panel4.setLocation((int)(width/3.5),(int)(height/2.561));
      add(panel4);
      
      menuBar = new JMenuBar();
      menuBar.setBackground(Color.WHITE);
   
      // File Menu, F - Mnemonic
      JMenu fileMenu = new JMenu("File");
//      JMenu optionsMenu = new JMenu("View");
//      optionsMenu.setMnemonic(KeyEvent.VK_O);
      fileMenu.setMnemonic(KeyEvent.VK_F);
      menuBar.add(fileMenu);
//      menuBar.add(optionsMenu);
   
    // File->Clear Calendar, C - Mnemonic
      JMenuItem clearCalendar = new JMenuItem("Clear Calendar", KeyEvent.VK_C);
      fileMenu.add(clearCalendar);
      
      clearCalendar.addActionListener(
         new ActionListener()
         {
            public void actionPerformed(ActionEvent e)
            { 
               table.clearData();
               table.repaint();
            }
         });
      
      JMenuItem newMenuItem8 = new JMenuItem("Change Password", KeyEvent.VK_W);
      fileMenu.add(newMenuItem8);
      
      newMenuItem8.addActionListener(
         new ActionListener()
         {
            public void actionPerformed(ActionEvent e)
            { 
               new SetPassword();
            }
         });
      
      JMenuItem newMenuItem2 = new JMenuItem("Help", KeyEvent.VK_H);
      fileMenu.add(newMenuItem2);
      
      JMenuItem newMenuItem7 = new JMenuItem("Print", KeyEvent.VK_P);
      fileMenu.add(newMenuItem7);
      
      newMenuItem7.addActionListener(
         new ActionListener()
         {
            public void actionPerformed(ActionEvent e)
            {
               try {
                  table.print();
               } 
               catch (PrinterException pe) {
                  System.err.println("Error printing: " + pe.getMessage());
               }
            }
         });
         
      JMenuItem settingsItem = new JMenuItem("Settings", KeyEvent.VK_P);
      fileMenu.add(settingsItem);
      
      settingsItem.addActionListener(
         new ActionListener()
         {
            public void actionPerformed(ActionEvent e)
            {
               new Settings(Visual.this);
            }
         });
      
      textField = new JTextArea();
      
      textField.setText("TODO: WRITE HELP FILE");  //TODO: Write help file
      
      textField.setLineWrap(true);
      textField.setFont(new java.awt.Font("TIMES NEW ROMAN", Font.ITALIC | Font.BOLD, 14));
      textField.setForeground(Color.BLACK);
      textField.setBackground(Color.WHITE);
      //textField.setVisible(false);
      textField.setColumns(20);
      textField.setEditable(false);
      textField.setWrapStyleWord(true);
      panel4.setVisible(false);
      panel4.add(textField);
      X = new JButton("X");
      X.setForeground(Color.RED);
      X.setLocation((int)(width/1.6725),(int)(height/2.76));
      X.setSize((int)width/45,(int)height/35);
      X.setVisible(false);
      add(X);
      
      newMenuItem2.addActionListener(
         new ActionListener()
         {
            public void actionPerformed(ActionEvent e)
            {
               panel4.setVisible(true);
               calContainer.setVisible(false);
               X.setVisible(true);
               X.addActionListener(
                  new ActionListener() {
                     public void actionPerformed(ActionEvent ae) {
                        panel4.setVisible(false);
                        calContainer.setVisible(true);
                        X.setVisible(false);
                     }
                  });   
            }
         });
      
      menuBar.setOpaque(true);
      menuBar.setVisible(true);
      add(menuBar);
      /////////////////////////////////////////////////////////////////////////////////////////////////
   }
}